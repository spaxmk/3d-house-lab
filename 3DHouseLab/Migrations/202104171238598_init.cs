﻿namespace _3DHouseLab.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Kupacs",
                c => new
                    {
                        KupacId = c.Int(nullable: false, identity: true),
                        Ime = c.String(maxLength: 255),
                        Prezime = c.String(maxLength: 255),
                        Adresa = c.String(maxLength: 255),
                        Grad = c.String(maxLength: 255),
                        PTT = c.Int(nullable: false),
                        BrTelefona = c.String(maxLength: 10),
                        DrustvenaMreza = c.String(maxLength: 255),
                        ImeNaDrustvenojMrezi = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.KupacId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Kupacs");
        }
    }
}
