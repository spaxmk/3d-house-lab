﻿namespace _3DHouseLab.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateKupac : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Kupacs", "BrTelefona", c => c.String(maxLength: 11));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Kupacs", "BrTelefona", c => c.String(maxLength: 10));
        }
    }
}
