﻿namespace _3DHouseLab.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Porudzbenicainit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Porudzbenicas",
                c => new
                    {
                        PorudzbenicaId = c.Int(nullable: false, identity: true),
                        Spisak = c.String(),
                        UkupnaCena = c.Int(nullable: false),
                        DatumPorudzbine = c.String(),
                        DatumSlanja = c.String(),
                        IznosIsplacen = c.Boolean(nullable: false),
                        IznosPodignut = c.Boolean(nullable: false),
                        KupacId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PorudzbenicaId)
                .ForeignKey("dbo.Kupacs", t => t.KupacId, cascadeDelete: true)
                .Index(t => t.KupacId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Porudzbenicas", "KupacId", "dbo.Kupacs");
            DropIndex("dbo.Porudzbenicas", new[] { "KupacId" });
            DropTable("dbo.Porudzbenicas");
        }
    }
}
