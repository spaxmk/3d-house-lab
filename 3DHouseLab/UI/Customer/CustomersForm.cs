﻿using _3DHouseLab.Models.db;
using _3DHouseLab.UI;
using _3DHouseLab.UI.Customer;
using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using _3DHouseLab;
using static _3DHouseLab.Form3dHouseLab;
using _3DHouseLab.Models;

namespace _3DHouseLab.UI
{
    public partial class CustomersForm : Form
    {
        private OpenForms opnFrm = new OpenForms();

        public CustomersForm()
        {
            InitializeComponent();
            btnAddCustomer.Visible = false;

        }

        //Load from db
        private void CustomersForm_Load(object sender, EventArgs e)
        {
            using (ModelsContextDb db = new ModelsContextDb())
            {
                kupacBindingSource.DataSource = db.ListaKupaca.ToList();
            }
            PanelCustomersForm.Enabled = true;

            Kupac kupac = kupacBindingSource.Current as Kupac;
            
        }

        //Add new Form 'AddNewCustomer'
        private void btnAddNewCustomer_Click(object sender, EventArgs e)
        {
            ChangeVisibility();

            kupacBindingSource.Add(new Kupac());
            kupacBindingSource.MoveLast();
            txtCustomerFirstName.Focus();
        }

        private void btnAddCustomer_Click(object sender, EventArgs e)
        {
            
            using (ModelsContextDb db = new ModelsContextDb())
            {
                Kupac kupac = kupacBindingSource.Current as Kupac;
                if (kupac != null)
                {
                    if (db.Entry<Kupac>(kupac).State == System.Data.Entity.EntityState.Detached)
                        db.Set<Kupac>().Attach(kupac);
                    if (kupac.KupacId == 0)
                        db.Entry<Kupac>(kupac).State = System.Data.Entity.EntityState.Added;
                    else
                        db.Entry<Kupac>(kupac).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    metroGrid1.Refresh();
                    ChangeVisibility();
                }
            }
        }

        private void ChangeVisibility()
        {
            if(btnAddNewCustomer.Visible == true)
            {
                btnAddNewCustomer.Visible = false;
                btnAddCustomer.Visible = true;
                PanelCustomersForm.Enabled = true;
                btnCustomerEdit.Visible = false;
                btnCustomerDelete.Visible = false;
            }
            else
            {
                btnAddCustomer.Visible = false;
                btnCustomerEdit.Visible = true;
                btnCustomerDelete.Visible = true;
                btnAddNewCustomer.Visible = true;
            }
        }

        private void btnCustomerDelete_Click(object sender, EventArgs e)
        {
            if(MetroFramework.MetroMessageBox.Show(this, "Da li ste sigurni?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                using (ModelsContextDb db = new ModelsContextDb())
                {
                    Kupac kupac = kupacBindingSource.Current as Kupac;
                    if(kupac != null)
                    {
                        if(db.Entry<Kupac>(kupac).State == System.Data.Entity.EntityState.Detached)
                        {
                            db.Set<Kupac>().Attach(kupac);
                        }
                        db.Entry<Kupac>(kupac).State = System.Data.Entity.EntityState.Deleted;
                        db.SaveChanges();
                        kupacBindingSource.RemoveCurrent();

                    }
                }
            }
        }

        private void btnCustomerEdit_Click(object sender, EventArgs e)
        {
            using(ModelsContextDb db = new ModelsContextDb())
            {
                Kupac kupac = kupacBindingSource.Current as Kupac;
                if(kupac != null)
                {
                    if(db.Entry<Kupac>(kupac).State == System.Data.Entity.EntityState.Detached)
                    {
                        db.Set<Kupac>().Attach(kupac);
                    }
                    if (kupac.KupacId == 0)
                        db.Entry<Kupac>(kupac).State = System.Data.Entity.EntityState.Added;
                    else
                        db.Entry<Kupac>(kupac).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    metroGrid1.Refresh();
                }
            }
        }

        private void metroGrid1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            OrdersForm childForm = new OrdersForm();
            opnFrm.OpenChildForm(childForm, PanelCustomersForm);
        }
    }
}
