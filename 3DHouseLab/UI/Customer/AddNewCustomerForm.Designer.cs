﻿
namespace _3DHouseLab.UI.Customer
{
    partial class AddNewCustomerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AddNewCustomerPanel = new MetroFramework.Controls.MetroPanel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.txtCustomerFirstName = new MetroFramework.Controls.MetroTextBox();
            this.txtCustomerLastName = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.txtCustomerAddress = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.txtCustomerCity = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.txtCustomerPtt = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.txtCustomerPhone = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroComboBox1 = new MetroFramework.Controls.MetroComboBox();
            this.btnAddCustomer = new MetroFramework.Controls.MetroButton();
            this.AddNewCustomerPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // AddNewCustomerPanel
            // 
            this.AddNewCustomerPanel.Controls.Add(this.btnAddCustomer);
            this.AddNewCustomerPanel.Controls.Add(this.metroComboBox1);
            this.AddNewCustomerPanel.Controls.Add(this.metroLabel7);
            this.AddNewCustomerPanel.Controls.Add(this.txtCustomerPhone);
            this.AddNewCustomerPanel.Controls.Add(this.metroLabel6);
            this.AddNewCustomerPanel.Controls.Add(this.txtCustomerPtt);
            this.AddNewCustomerPanel.Controls.Add(this.metroLabel5);
            this.AddNewCustomerPanel.Controls.Add(this.txtCustomerCity);
            this.AddNewCustomerPanel.Controls.Add(this.metroLabel4);
            this.AddNewCustomerPanel.Controls.Add(this.txtCustomerAddress);
            this.AddNewCustomerPanel.Controls.Add(this.metroLabel3);
            this.AddNewCustomerPanel.Controls.Add(this.txtCustomerLastName);
            this.AddNewCustomerPanel.Controls.Add(this.metroLabel2);
            this.AddNewCustomerPanel.Controls.Add(this.txtCustomerFirstName);
            this.AddNewCustomerPanel.Controls.Add(this.metroLabel1);
            this.AddNewCustomerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AddNewCustomerPanel.HorizontalScrollbarBarColor = true;
            this.AddNewCustomerPanel.HorizontalScrollbarHighlightOnWheel = false;
            this.AddNewCustomerPanel.HorizontalScrollbarSize = 10;
            this.AddNewCustomerPanel.Location = new System.Drawing.Point(0, 0);
            this.AddNewCustomerPanel.Name = "AddNewCustomerPanel";
            this.AddNewCustomerPanel.Size = new System.Drawing.Size(1064, 681);
            this.AddNewCustomerPanel.TabIndex = 0;
            this.AddNewCustomerPanel.VerticalScrollbarBarColor = true;
            this.AddNewCustomerPanel.VerticalScrollbarHighlightOnWheel = false;
            this.AddNewCustomerPanel.VerticalScrollbarSize = 10;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(24, 27);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(34, 19);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "Ime:";
            // 
            // txtCustomerFirstName
            // 
            // 
            // 
            // 
            this.txtCustomerFirstName.CustomButton.Image = null;
            this.txtCustomerFirstName.CustomButton.Location = new System.Drawing.Point(138, 1);
            this.txtCustomerFirstName.CustomButton.Name = "";
            this.txtCustomerFirstName.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.txtCustomerFirstName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtCustomerFirstName.CustomButton.TabIndex = 1;
            this.txtCustomerFirstName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtCustomerFirstName.CustomButton.UseSelectable = true;
            this.txtCustomerFirstName.CustomButton.Visible = false;
            this.txtCustomerFirstName.Lines = new string[0];
            this.txtCustomerFirstName.Location = new System.Drawing.Point(24, 49);
            this.txtCustomerFirstName.MaxLength = 32767;
            this.txtCustomerFirstName.Name = "txtCustomerFirstName";
            this.txtCustomerFirstName.PasswordChar = '\0';
            this.txtCustomerFirstName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCustomerFirstName.SelectedText = "";
            this.txtCustomerFirstName.SelectionLength = 0;
            this.txtCustomerFirstName.SelectionStart = 0;
            this.txtCustomerFirstName.ShortcutsEnabled = true;
            this.txtCustomerFirstName.Size = new System.Drawing.Size(166, 29);
            this.txtCustomerFirstName.TabIndex = 3;
            this.txtCustomerFirstName.UseSelectable = true;
            this.txtCustomerFirstName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtCustomerFirstName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtCustomerLastName
            // 
            // 
            // 
            // 
            this.txtCustomerLastName.CustomButton.Image = null;
            this.txtCustomerLastName.CustomButton.Location = new System.Drawing.Point(138, 1);
            this.txtCustomerLastName.CustomButton.Name = "";
            this.txtCustomerLastName.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.txtCustomerLastName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtCustomerLastName.CustomButton.TabIndex = 1;
            this.txtCustomerLastName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtCustomerLastName.CustomButton.UseSelectable = true;
            this.txtCustomerLastName.CustomButton.Visible = false;
            this.txtCustomerLastName.Lines = new string[0];
            this.txtCustomerLastName.Location = new System.Drawing.Point(24, 116);
            this.txtCustomerLastName.MaxLength = 32767;
            this.txtCustomerLastName.Name = "txtCustomerLastName";
            this.txtCustomerLastName.PasswordChar = '\0';
            this.txtCustomerLastName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCustomerLastName.SelectedText = "";
            this.txtCustomerLastName.SelectionLength = 0;
            this.txtCustomerLastName.SelectionStart = 0;
            this.txtCustomerLastName.ShortcutsEnabled = true;
            this.txtCustomerLastName.Size = new System.Drawing.Size(166, 29);
            this.txtCustomerLastName.TabIndex = 5;
            this.txtCustomerLastName.UseSelectable = true;
            this.txtCustomerLastName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtCustomerLastName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(24, 94);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(60, 19);
            this.metroLabel2.TabIndex = 4;
            this.metroLabel2.Text = "Prezime:";
            // 
            // txtCustomerAddress
            // 
            // 
            // 
            // 
            this.txtCustomerAddress.CustomButton.Image = null;
            this.txtCustomerAddress.CustomButton.Location = new System.Drawing.Point(138, 1);
            this.txtCustomerAddress.CustomButton.Name = "";
            this.txtCustomerAddress.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.txtCustomerAddress.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtCustomerAddress.CustomButton.TabIndex = 1;
            this.txtCustomerAddress.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtCustomerAddress.CustomButton.UseSelectable = true;
            this.txtCustomerAddress.CustomButton.Visible = false;
            this.txtCustomerAddress.Lines = new string[0];
            this.txtCustomerAddress.Location = new System.Drawing.Point(24, 184);
            this.txtCustomerAddress.MaxLength = 32767;
            this.txtCustomerAddress.Name = "txtCustomerAddress";
            this.txtCustomerAddress.PasswordChar = '\0';
            this.txtCustomerAddress.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCustomerAddress.SelectedText = "";
            this.txtCustomerAddress.SelectionLength = 0;
            this.txtCustomerAddress.SelectionStart = 0;
            this.txtCustomerAddress.ShortcutsEnabled = true;
            this.txtCustomerAddress.Size = new System.Drawing.Size(166, 29);
            this.txtCustomerAddress.TabIndex = 7;
            this.txtCustomerAddress.UseSelectable = true;
            this.txtCustomerAddress.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtCustomerAddress.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(24, 162);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(53, 19);
            this.metroLabel3.TabIndex = 6;
            this.metroLabel3.Text = "Adresa:";
            // 
            // txtCustomerCity
            // 
            // 
            // 
            // 
            this.txtCustomerCity.CustomButton.Image = null;
            this.txtCustomerCity.CustomButton.Location = new System.Drawing.Point(138, 1);
            this.txtCustomerCity.CustomButton.Name = "";
            this.txtCustomerCity.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.txtCustomerCity.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtCustomerCity.CustomButton.TabIndex = 1;
            this.txtCustomerCity.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtCustomerCity.CustomButton.UseSelectable = true;
            this.txtCustomerCity.CustomButton.Visible = false;
            this.txtCustomerCity.Lines = new string[0];
            this.txtCustomerCity.Location = new System.Drawing.Point(24, 247);
            this.txtCustomerCity.MaxLength = 32767;
            this.txtCustomerCity.Name = "txtCustomerCity";
            this.txtCustomerCity.PasswordChar = '\0';
            this.txtCustomerCity.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCustomerCity.SelectedText = "";
            this.txtCustomerCity.SelectionLength = 0;
            this.txtCustomerCity.SelectionStart = 0;
            this.txtCustomerCity.ShortcutsEnabled = true;
            this.txtCustomerCity.Size = new System.Drawing.Size(166, 29);
            this.txtCustomerCity.TabIndex = 9;
            this.txtCustomerCity.UseSelectable = true;
            this.txtCustomerCity.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtCustomerCity.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(24, 225);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(41, 19);
            this.metroLabel4.TabIndex = 8;
            this.metroLabel4.Text = "Grad:";
            // 
            // txtCustomerPtt
            // 
            // 
            // 
            // 
            this.txtCustomerPtt.CustomButton.Image = null;
            this.txtCustomerPtt.CustomButton.Location = new System.Drawing.Point(138, 1);
            this.txtCustomerPtt.CustomButton.Name = "";
            this.txtCustomerPtt.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.txtCustomerPtt.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtCustomerPtt.CustomButton.TabIndex = 1;
            this.txtCustomerPtt.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtCustomerPtt.CustomButton.UseSelectable = true;
            this.txtCustomerPtt.CustomButton.Visible = false;
            this.txtCustomerPtt.Lines = new string[0];
            this.txtCustomerPtt.Location = new System.Drawing.Point(24, 311);
            this.txtCustomerPtt.MaxLength = 32767;
            this.txtCustomerPtt.Name = "txtCustomerPtt";
            this.txtCustomerPtt.PasswordChar = '\0';
            this.txtCustomerPtt.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCustomerPtt.SelectedText = "";
            this.txtCustomerPtt.SelectionLength = 0;
            this.txtCustomerPtt.SelectionStart = 0;
            this.txtCustomerPtt.ShortcutsEnabled = true;
            this.txtCustomerPtt.Size = new System.Drawing.Size(166, 29);
            this.txtCustomerPtt.TabIndex = 11;
            this.txtCustomerPtt.UseSelectable = true;
            this.txtCustomerPtt.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtCustomerPtt.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(24, 289);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(34, 19);
            this.metroLabel5.TabIndex = 10;
            this.metroLabel5.Text = "PTT:";
            // 
            // txtCustomerPhone
            // 
            // 
            // 
            // 
            this.txtCustomerPhone.CustomButton.Image = null;
            this.txtCustomerPhone.CustomButton.Location = new System.Drawing.Point(138, 1);
            this.txtCustomerPhone.CustomButton.Name = "";
            this.txtCustomerPhone.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.txtCustomerPhone.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtCustomerPhone.CustomButton.TabIndex = 1;
            this.txtCustomerPhone.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtCustomerPhone.CustomButton.UseSelectable = true;
            this.txtCustomerPhone.CustomButton.Visible = false;
            this.txtCustomerPhone.Lines = new string[0];
            this.txtCustomerPhone.Location = new System.Drawing.Point(24, 384);
            this.txtCustomerPhone.MaxLength = 32767;
            this.txtCustomerPhone.Name = "txtCustomerPhone";
            this.txtCustomerPhone.PasswordChar = '\0';
            this.txtCustomerPhone.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCustomerPhone.SelectedText = "";
            this.txtCustomerPhone.SelectionLength = 0;
            this.txtCustomerPhone.SelectionStart = 0;
            this.txtCustomerPhone.ShortcutsEnabled = true;
            this.txtCustomerPhone.Size = new System.Drawing.Size(166, 29);
            this.txtCustomerPhone.TabIndex = 13;
            this.txtCustomerPhone.UseSelectable = true;
            this.txtCustomerPhone.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtCustomerPhone.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(24, 362);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(53, 19);
            this.metroLabel6.TabIndex = 12;
            this.metroLabel6.Text = "Telefon:";
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(24, 432);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(110, 19);
            this.metroLabel7.TabIndex = 14;
            this.metroLabel7.Text = "Drustvena Mreza:";
            // 
            // metroComboBox1
            // 
            this.metroComboBox1.FormattingEnabled = true;
            this.metroComboBox1.ItemHeight = 23;
            this.metroComboBox1.Location = new System.Drawing.Point(24, 454);
            this.metroComboBox1.Name = "metroComboBox1";
            this.metroComboBox1.Size = new System.Drawing.Size(166, 29);
            this.metroComboBox1.TabIndex = 15;
            this.metroComboBox1.UseSelectable = true;
            // 
            // btnAddCustomer
            // 
            this.btnAddCustomer.Location = new System.Drawing.Point(42, 503);
            this.btnAddCustomer.Name = "btnAddCustomer";
            this.btnAddCustomer.Size = new System.Drawing.Size(115, 31);
            this.btnAddCustomer.TabIndex = 16;
            this.btnAddCustomer.Text = "Dodaj";
            this.btnAddCustomer.UseSelectable = true;
            this.btnAddCustomer.Click += new System.EventHandler(this.btnAddCustomer_Click);
            // 
            // AddNewCustomerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1064, 681);
            this.Controls.Add(this.AddNewCustomerPanel);
            this.Name = "AddNewCustomerForm";
            this.Text = "AddNewCustomer";
            this.AddNewCustomerPanel.ResumeLayout(false);
            this.AddNewCustomerPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel AddNewCustomerPanel;
        private MetroFramework.Controls.MetroComboBox metroComboBox1;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroTextBox txtCustomerPhone;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroTextBox txtCustomerPtt;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroTextBox txtCustomerCity;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroTextBox txtCustomerAddress;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox txtCustomerLastName;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox txtCustomerFirstName;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroButton btnAddCustomer;
    }
}