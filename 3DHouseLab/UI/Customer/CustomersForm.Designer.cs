﻿
namespace _3DHouseLab.UI
{
    partial class CustomersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PanelCustomersForm = new MetroFramework.Controls.MetroPanel();
            this.btnCustomerDelete = new MetroFramework.Controls.MetroButton();
            this.btnCustomerEdit = new MetroFramework.Controls.MetroButton();
            this.txtNickname = new MetroFramework.Controls.MetroTextBox();
            this.kupacBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.btnAddCustomer = new MetroFramework.Controls.MetroButton();
            this.metroComboBox1 = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.txtCustomerPhone = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.txtCustomerPtt = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.txtCustomerCity = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.txtCustomerAddress = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.txtCustomerLastName = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.txtCustomerFirstName = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.btnAddNewCustomer = new MetroFramework.Controls.MetroButton();
            this.txtSearch = new MetroFramework.Controls.MetroTextBox();
            this.metroGrid1 = new MetroFramework.Controls.MetroGrid();
            this.imeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prezimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.adresaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gradDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pTTDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.brTelefonaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.drustvenaMrezaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.imeNaDrustvenojMreziDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PanelCustomersForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kupacBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelCustomersForm
            // 
            this.PanelCustomersForm.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelCustomersForm.Controls.Add(this.btnCustomerDelete);
            this.PanelCustomersForm.Controls.Add(this.btnCustomerEdit);
            this.PanelCustomersForm.Controls.Add(this.txtNickname);
            this.PanelCustomersForm.Controls.Add(this.metroLabel8);
            this.PanelCustomersForm.Controls.Add(this.btnAddCustomer);
            this.PanelCustomersForm.Controls.Add(this.metroComboBox1);
            this.PanelCustomersForm.Controls.Add(this.metroLabel7);
            this.PanelCustomersForm.Controls.Add(this.txtCustomerPhone);
            this.PanelCustomersForm.Controls.Add(this.metroLabel6);
            this.PanelCustomersForm.Controls.Add(this.txtCustomerPtt);
            this.PanelCustomersForm.Controls.Add(this.metroLabel5);
            this.PanelCustomersForm.Controls.Add(this.txtCustomerCity);
            this.PanelCustomersForm.Controls.Add(this.metroLabel4);
            this.PanelCustomersForm.Controls.Add(this.txtCustomerAddress);
            this.PanelCustomersForm.Controls.Add(this.metroLabel3);
            this.PanelCustomersForm.Controls.Add(this.txtCustomerLastName);
            this.PanelCustomersForm.Controls.Add(this.metroLabel2);
            this.PanelCustomersForm.Controls.Add(this.txtCustomerFirstName);
            this.PanelCustomersForm.Controls.Add(this.metroLabel1);
            this.PanelCustomersForm.Controls.Add(this.metroButton1);
            this.PanelCustomersForm.Controls.Add(this.btnAddNewCustomer);
            this.PanelCustomersForm.Controls.Add(this.txtSearch);
            this.PanelCustomersForm.Controls.Add(this.metroGrid1);
            this.PanelCustomersForm.HorizontalScrollbarBarColor = true;
            this.PanelCustomersForm.HorizontalScrollbarHighlightOnWheel = false;
            this.PanelCustomersForm.HorizontalScrollbarSize = 10;
            this.PanelCustomersForm.Location = new System.Drawing.Point(12, 12);
            this.PanelCustomersForm.Name = "PanelCustomersForm";
            this.PanelCustomersForm.Size = new System.Drawing.Size(1040, 657);
            this.PanelCustomersForm.TabIndex = 0;
            this.PanelCustomersForm.VerticalScrollbarBarColor = true;
            this.PanelCustomersForm.VerticalScrollbarHighlightOnWheel = false;
            this.PanelCustomersForm.VerticalScrollbarSize = 10;
            // 
            // btnCustomerDelete
            // 
            this.btnCustomerDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCustomerDelete.Location = new System.Drawing.Point(923, 610);
            this.btnCustomerDelete.Name = "btnCustomerDelete";
            this.btnCustomerDelete.Size = new System.Drawing.Size(114, 44);
            this.btnCustomerDelete.TabIndex = 35;
            this.btnCustomerDelete.Text = "Obrisi";
            this.btnCustomerDelete.UseSelectable = true;
            this.btnCustomerDelete.Click += new System.EventHandler(this.btnCustomerDelete_Click);
            // 
            // btnCustomerEdit
            // 
            this.btnCustomerEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCustomerEdit.Location = new System.Drawing.Point(803, 610);
            this.btnCustomerEdit.Name = "btnCustomerEdit";
            this.btnCustomerEdit.Size = new System.Drawing.Size(114, 44);
            this.btnCustomerEdit.TabIndex = 34;
            this.btnCustomerEdit.Text = "Izmeni";
            this.btnCustomerEdit.UseSelectable = true;
            this.btnCustomerEdit.Click += new System.EventHandler(this.btnCustomerEdit_Click);
            // 
            // txtNickname
            // 
            // 
            // 
            // 
            this.txtNickname.CustomButton.Image = null;
            this.txtNickname.CustomButton.Location = new System.Drawing.Point(148, 1);
            this.txtNickname.CustomButton.Name = "";
            this.txtNickname.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.txtNickname.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNickname.CustomButton.TabIndex = 1;
            this.txtNickname.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNickname.CustomButton.UseSelectable = true;
            this.txtNickname.CustomButton.Visible = false;
            this.txtNickname.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.kupacBindingSource, "ImeNaDrustvenojMrezi", true));
            this.txtNickname.Enabled = false;
            this.txtNickname.Lines = new string[0];
            this.txtNickname.Location = new System.Drawing.Point(12, 566);
            this.txtNickname.MaxLength = 32767;
            this.txtNickname.Name = "txtNickname";
            this.txtNickname.PasswordChar = '\0';
            this.txtNickname.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNickname.SelectedText = "";
            this.txtNickname.SelectionLength = 0;
            this.txtNickname.SelectionStart = 0;
            this.txtNickname.ShortcutsEnabled = true;
            this.txtNickname.Size = new System.Drawing.Size(176, 29);
            this.txtNickname.TabIndex = 33;
            this.txtNickname.UseSelectable = true;
            this.txtNickname.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNickname.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // kupacBindingSource
            // 
            this.kupacBindingSource.DataSource = typeof(_3DHouseLab.Models.Kupac);
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Enabled = false;
            this.metroLabel8.Location = new System.Drawing.Point(12, 544);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(153, 19);
            this.metroLabel8.TabIndex = 32;
            this.metroLabel8.Text = "Ime na drustvenoj mrezi:";
            // 
            // btnAddCustomer
            // 
            this.btnAddCustomer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddCustomer.Location = new System.Drawing.Point(683, 610);
            this.btnAddCustomer.Name = "btnAddCustomer";
            this.btnAddCustomer.Size = new System.Drawing.Size(114, 44);
            this.btnAddCustomer.TabIndex = 31;
            this.btnAddCustomer.Text = "Dodaj";
            this.btnAddCustomer.UseSelectable = true;
            this.btnAddCustomer.Click += new System.EventHandler(this.btnAddCustomer_Click);
            // 
            // metroComboBox1
            // 
            this.metroComboBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.kupacBindingSource, "DrustvenaMreza", true));
            this.metroComboBox1.Enabled = false;
            this.metroComboBox1.FormattingEnabled = true;
            this.metroComboBox1.ItemHeight = 23;
            this.metroComboBox1.Items.AddRange(new object[] {
            "Instagram",
            "Fejs"});
            this.metroComboBox1.Location = new System.Drawing.Point(12, 497);
            this.metroComboBox1.Name = "metroComboBox1";
            this.metroComboBox1.Size = new System.Drawing.Size(176, 29);
            this.metroComboBox1.TabIndex = 30;
            this.metroComboBox1.UseSelectable = true;
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Enabled = false;
            this.metroLabel7.Location = new System.Drawing.Point(12, 475);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(110, 19);
            this.metroLabel7.TabIndex = 29;
            this.metroLabel7.Text = "Drustvena Mreza:";
            // 
            // txtCustomerPhone
            // 
            // 
            // 
            // 
            this.txtCustomerPhone.CustomButton.Image = null;
            this.txtCustomerPhone.CustomButton.Location = new System.Drawing.Point(148, 1);
            this.txtCustomerPhone.CustomButton.Name = "";
            this.txtCustomerPhone.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.txtCustomerPhone.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtCustomerPhone.CustomButton.TabIndex = 1;
            this.txtCustomerPhone.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtCustomerPhone.CustomButton.UseSelectable = true;
            this.txtCustomerPhone.CustomButton.Visible = false;
            this.txtCustomerPhone.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.kupacBindingSource, "BrTelefona", true));
            this.txtCustomerPhone.Enabled = false;
            this.txtCustomerPhone.Lines = new string[0];
            this.txtCustomerPhone.Location = new System.Drawing.Point(12, 427);
            this.txtCustomerPhone.MaxLength = 32767;
            this.txtCustomerPhone.Name = "txtCustomerPhone";
            this.txtCustomerPhone.PasswordChar = '\0';
            this.txtCustomerPhone.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCustomerPhone.SelectedText = "";
            this.txtCustomerPhone.SelectionLength = 0;
            this.txtCustomerPhone.SelectionStart = 0;
            this.txtCustomerPhone.ShortcutsEnabled = true;
            this.txtCustomerPhone.Size = new System.Drawing.Size(176, 29);
            this.txtCustomerPhone.TabIndex = 28;
            this.txtCustomerPhone.UseSelectable = true;
            this.txtCustomerPhone.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtCustomerPhone.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Enabled = false;
            this.metroLabel6.Location = new System.Drawing.Point(12, 405);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(53, 19);
            this.metroLabel6.TabIndex = 27;
            this.metroLabel6.Text = "Telefon:";
            // 
            // txtCustomerPtt
            // 
            // 
            // 
            // 
            this.txtCustomerPtt.CustomButton.Image = null;
            this.txtCustomerPtt.CustomButton.Location = new System.Drawing.Point(148, 1);
            this.txtCustomerPtt.CustomButton.Name = "";
            this.txtCustomerPtt.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.txtCustomerPtt.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtCustomerPtt.CustomButton.TabIndex = 1;
            this.txtCustomerPtt.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtCustomerPtt.CustomButton.UseSelectable = true;
            this.txtCustomerPtt.CustomButton.Visible = false;
            this.txtCustomerPtt.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.kupacBindingSource, "PTT", true));
            this.txtCustomerPtt.Enabled = false;
            this.txtCustomerPtt.Lines = new string[0];
            this.txtCustomerPtt.Location = new System.Drawing.Point(12, 354);
            this.txtCustomerPtt.MaxLength = 32767;
            this.txtCustomerPtt.Name = "txtCustomerPtt";
            this.txtCustomerPtt.PasswordChar = '\0';
            this.txtCustomerPtt.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCustomerPtt.SelectedText = "";
            this.txtCustomerPtt.SelectionLength = 0;
            this.txtCustomerPtt.SelectionStart = 0;
            this.txtCustomerPtt.ShortcutsEnabled = true;
            this.txtCustomerPtt.Size = new System.Drawing.Size(176, 29);
            this.txtCustomerPtt.TabIndex = 26;
            this.txtCustomerPtt.UseSelectable = true;
            this.txtCustomerPtt.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtCustomerPtt.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Enabled = false;
            this.metroLabel5.Location = new System.Drawing.Point(12, 332);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(34, 19);
            this.metroLabel5.TabIndex = 25;
            this.metroLabel5.Text = "PTT:";
            // 
            // txtCustomerCity
            // 
            // 
            // 
            // 
            this.txtCustomerCity.CustomButton.Image = null;
            this.txtCustomerCity.CustomButton.Location = new System.Drawing.Point(148, 1);
            this.txtCustomerCity.CustomButton.Name = "";
            this.txtCustomerCity.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.txtCustomerCity.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtCustomerCity.CustomButton.TabIndex = 1;
            this.txtCustomerCity.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtCustomerCity.CustomButton.UseSelectable = true;
            this.txtCustomerCity.CustomButton.Visible = false;
            this.txtCustomerCity.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.kupacBindingSource, "Grad", true));
            this.txtCustomerCity.Enabled = false;
            this.txtCustomerCity.Lines = new string[0];
            this.txtCustomerCity.Location = new System.Drawing.Point(12, 290);
            this.txtCustomerCity.MaxLength = 32767;
            this.txtCustomerCity.Name = "txtCustomerCity";
            this.txtCustomerCity.PasswordChar = '\0';
            this.txtCustomerCity.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCustomerCity.SelectedText = "";
            this.txtCustomerCity.SelectionLength = 0;
            this.txtCustomerCity.SelectionStart = 0;
            this.txtCustomerCity.ShortcutsEnabled = true;
            this.txtCustomerCity.Size = new System.Drawing.Size(176, 29);
            this.txtCustomerCity.TabIndex = 24;
            this.txtCustomerCity.UseSelectable = true;
            this.txtCustomerCity.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtCustomerCity.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Enabled = false;
            this.metroLabel4.Location = new System.Drawing.Point(12, 268);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(41, 19);
            this.metroLabel4.TabIndex = 23;
            this.metroLabel4.Text = "Grad:";
            // 
            // txtCustomerAddress
            // 
            // 
            // 
            // 
            this.txtCustomerAddress.CustomButton.Image = null;
            this.txtCustomerAddress.CustomButton.Location = new System.Drawing.Point(148, 1);
            this.txtCustomerAddress.CustomButton.Name = "";
            this.txtCustomerAddress.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.txtCustomerAddress.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtCustomerAddress.CustomButton.TabIndex = 1;
            this.txtCustomerAddress.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtCustomerAddress.CustomButton.UseSelectable = true;
            this.txtCustomerAddress.CustomButton.Visible = false;
            this.txtCustomerAddress.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.kupacBindingSource, "Adresa", true));
            this.txtCustomerAddress.Enabled = false;
            this.txtCustomerAddress.Lines = new string[0];
            this.txtCustomerAddress.Location = new System.Drawing.Point(12, 227);
            this.txtCustomerAddress.MaxLength = 32767;
            this.txtCustomerAddress.Name = "txtCustomerAddress";
            this.txtCustomerAddress.PasswordChar = '\0';
            this.txtCustomerAddress.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCustomerAddress.SelectedText = "";
            this.txtCustomerAddress.SelectionLength = 0;
            this.txtCustomerAddress.SelectionStart = 0;
            this.txtCustomerAddress.ShortcutsEnabled = true;
            this.txtCustomerAddress.Size = new System.Drawing.Size(176, 29);
            this.txtCustomerAddress.TabIndex = 22;
            this.txtCustomerAddress.UseSelectable = true;
            this.txtCustomerAddress.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtCustomerAddress.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Enabled = false;
            this.metroLabel3.Location = new System.Drawing.Point(12, 205);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(53, 19);
            this.metroLabel3.TabIndex = 21;
            this.metroLabel3.Text = "Adresa:";
            // 
            // txtCustomerLastName
            // 
            // 
            // 
            // 
            this.txtCustomerLastName.CustomButton.Image = null;
            this.txtCustomerLastName.CustomButton.Location = new System.Drawing.Point(148, 1);
            this.txtCustomerLastName.CustomButton.Name = "";
            this.txtCustomerLastName.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.txtCustomerLastName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtCustomerLastName.CustomButton.TabIndex = 1;
            this.txtCustomerLastName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtCustomerLastName.CustomButton.UseSelectable = true;
            this.txtCustomerLastName.CustomButton.Visible = false;
            this.txtCustomerLastName.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.kupacBindingSource, "Prezime", true));
            this.txtCustomerLastName.Enabled = false;
            this.txtCustomerLastName.Lines = new string[0];
            this.txtCustomerLastName.Location = new System.Drawing.Point(12, 159);
            this.txtCustomerLastName.MaxLength = 32767;
            this.txtCustomerLastName.Name = "txtCustomerLastName";
            this.txtCustomerLastName.PasswordChar = '\0';
            this.txtCustomerLastName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCustomerLastName.SelectedText = "";
            this.txtCustomerLastName.SelectionLength = 0;
            this.txtCustomerLastName.SelectionStart = 0;
            this.txtCustomerLastName.ShortcutsEnabled = true;
            this.txtCustomerLastName.Size = new System.Drawing.Size(176, 29);
            this.txtCustomerLastName.TabIndex = 20;
            this.txtCustomerLastName.UseSelectable = true;
            this.txtCustomerLastName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtCustomerLastName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Enabled = false;
            this.metroLabel2.Location = new System.Drawing.Point(12, 137);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(60, 19);
            this.metroLabel2.TabIndex = 19;
            this.metroLabel2.Text = "Prezime:";
            // 
            // txtCustomerFirstName
            // 
            // 
            // 
            // 
            this.txtCustomerFirstName.CustomButton.Image = null;
            this.txtCustomerFirstName.CustomButton.Location = new System.Drawing.Point(148, 1);
            this.txtCustomerFirstName.CustomButton.Name = "";
            this.txtCustomerFirstName.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.txtCustomerFirstName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtCustomerFirstName.CustomButton.TabIndex = 1;
            this.txtCustomerFirstName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtCustomerFirstName.CustomButton.UseSelectable = true;
            this.txtCustomerFirstName.CustomButton.Visible = false;
            this.txtCustomerFirstName.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.kupacBindingSource, "Ime", true));
            this.txtCustomerFirstName.Enabled = false;
            this.txtCustomerFirstName.Lines = new string[0];
            this.txtCustomerFirstName.Location = new System.Drawing.Point(12, 92);
            this.txtCustomerFirstName.MaxLength = 32767;
            this.txtCustomerFirstName.Name = "txtCustomerFirstName";
            this.txtCustomerFirstName.PasswordChar = '\0';
            this.txtCustomerFirstName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCustomerFirstName.SelectedText = "";
            this.txtCustomerFirstName.SelectionLength = 0;
            this.txtCustomerFirstName.SelectionStart = 0;
            this.txtCustomerFirstName.ShortcutsEnabled = true;
            this.txtCustomerFirstName.Size = new System.Drawing.Size(176, 29);
            this.txtCustomerFirstName.TabIndex = 18;
            this.txtCustomerFirstName.UseSelectable = true;
            this.txtCustomerFirstName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtCustomerFirstName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Enabled = false;
            this.metroLabel1.Location = new System.Drawing.Point(12, 70);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(34, 19);
            this.metroLabel1.TabIndex = 17;
            this.metroLabel1.Text = "Ime:";
            // 
            // metroButton1
            // 
            this.metroButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.metroButton1.Location = new System.Drawing.Point(290, 610);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(114, 44);
            this.metroButton1.TabIndex = 5;
            this.metroButton1.Text = "Dodaj na crnu listu";
            this.metroButton1.UseSelectable = true;
            // 
            // btnAddNewCustomer
            // 
            this.btnAddNewCustomer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddNewCustomer.Location = new System.Drawing.Point(683, 610);
            this.btnAddNewCustomer.Name = "btnAddNewCustomer";
            this.btnAddNewCustomer.Size = new System.Drawing.Size(114, 44);
            this.btnAddNewCustomer.TabIndex = 4;
            this.btnAddNewCustomer.Text = "Dodaj  kupca";
            this.btnAddNewCustomer.UseSelectable = true;
            this.btnAddNewCustomer.Click += new System.EventHandler(this.btnAddNewCustomer_Click);
            // 
            // txtSearch
            // 
            // 
            // 
            // 
            this.txtSearch.CustomButton.Image = null;
            this.txtSearch.CustomButton.Location = new System.Drawing.Point(124, 1);
            this.txtSearch.CustomButton.Name = "";
            this.txtSearch.CustomButton.Size = new System.Drawing.Size(31, 31);
            this.txtSearch.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtSearch.CustomButton.TabIndex = 1;
            this.txtSearch.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtSearch.CustomButton.UseSelectable = true;
            this.txtSearch.CustomButton.Visible = false;
            this.txtSearch.Lines = new string[] {
        "Pretraga:"};
            this.txtSearch.Location = new System.Drawing.Point(224, 19);
            this.txtSearch.MaxLength = 32767;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.PasswordChar = '\0';
            this.txtSearch.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtSearch.SelectedText = "";
            this.txtSearch.SelectionLength = 0;
            this.txtSearch.SelectionStart = 0;
            this.txtSearch.ShortcutsEnabled = true;
            this.txtSearch.Size = new System.Drawing.Size(156, 33);
            this.txtSearch.TabIndex = 3;
            this.txtSearch.Text = "Pretraga:";
            this.txtSearch.UseSelectable = true;
            this.txtSearch.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtSearch.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroGrid1
            // 
            this.metroGrid1.AllowUserToResizeRows = false;
            this.metroGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroGrid1.AutoGenerateColumns = false;
            this.metroGrid1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGrid1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGrid1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.metroGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.metroGrid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.imeDataGridViewTextBoxColumn,
            this.prezimeDataGridViewTextBoxColumn,
            this.adresaDataGridViewTextBoxColumn,
            this.gradDataGridViewTextBoxColumn,
            this.pTTDataGridViewTextBoxColumn,
            this.brTelefonaDataGridViewTextBoxColumn,
            this.drustvenaMrezaDataGridViewTextBoxColumn,
            this.imeNaDrustvenojMreziDataGridViewTextBoxColumn});
            this.metroGrid1.DataSource = this.kupacBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGrid1.DefaultCellStyle = dataGridViewCellStyle2;
            this.metroGrid1.EnableHeadersVisualStyles = false;
            this.metroGrid1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGrid1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid1.Location = new System.Drawing.Point(194, 70);
            this.metroGrid1.Name = "metroGrid1";
            this.metroGrid1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.metroGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGrid1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGrid1.Size = new System.Drawing.Size(843, 534);
            this.metroGrid1.TabIndex = 2;
            this.metroGrid1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.metroGrid1_CellDoubleClick);
            // 
            // imeDataGridViewTextBoxColumn
            // 
            this.imeDataGridViewTextBoxColumn.DataPropertyName = "Ime";
            this.imeDataGridViewTextBoxColumn.HeaderText = "Ime";
            this.imeDataGridViewTextBoxColumn.Name = "imeDataGridViewTextBoxColumn";
            // 
            // prezimeDataGridViewTextBoxColumn
            // 
            this.prezimeDataGridViewTextBoxColumn.DataPropertyName = "Prezime";
            this.prezimeDataGridViewTextBoxColumn.HeaderText = "Prezime";
            this.prezimeDataGridViewTextBoxColumn.Name = "prezimeDataGridViewTextBoxColumn";
            // 
            // adresaDataGridViewTextBoxColumn
            // 
            this.adresaDataGridViewTextBoxColumn.DataPropertyName = "Adresa";
            this.adresaDataGridViewTextBoxColumn.HeaderText = "Adresa";
            this.adresaDataGridViewTextBoxColumn.Name = "adresaDataGridViewTextBoxColumn";
            // 
            // gradDataGridViewTextBoxColumn
            // 
            this.gradDataGridViewTextBoxColumn.DataPropertyName = "Grad";
            this.gradDataGridViewTextBoxColumn.HeaderText = "Grad";
            this.gradDataGridViewTextBoxColumn.Name = "gradDataGridViewTextBoxColumn";
            // 
            // pTTDataGridViewTextBoxColumn
            // 
            this.pTTDataGridViewTextBoxColumn.DataPropertyName = "PTT";
            this.pTTDataGridViewTextBoxColumn.HeaderText = "PTT";
            this.pTTDataGridViewTextBoxColumn.Name = "pTTDataGridViewTextBoxColumn";
            // 
            // brTelefonaDataGridViewTextBoxColumn
            // 
            this.brTelefonaDataGridViewTextBoxColumn.DataPropertyName = "BrTelefona";
            this.brTelefonaDataGridViewTextBoxColumn.HeaderText = "Br Telefona";
            this.brTelefonaDataGridViewTextBoxColumn.Name = "brTelefonaDataGridViewTextBoxColumn";
            // 
            // drustvenaMrezaDataGridViewTextBoxColumn
            // 
            this.drustvenaMrezaDataGridViewTextBoxColumn.DataPropertyName = "DrustvenaMreza";
            this.drustvenaMrezaDataGridViewTextBoxColumn.HeaderText = "Drustvena Mreza";
            this.drustvenaMrezaDataGridViewTextBoxColumn.Name = "drustvenaMrezaDataGridViewTextBoxColumn";
            this.drustvenaMrezaDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // imeNaDrustvenojMreziDataGridViewTextBoxColumn
            // 
            this.imeNaDrustvenojMreziDataGridViewTextBoxColumn.DataPropertyName = "ImeNaDrustvenojMrezi";
            this.imeNaDrustvenojMreziDataGridViewTextBoxColumn.HeaderText = "Nickname";
            this.imeNaDrustvenojMreziDataGridViewTextBoxColumn.Name = "imeNaDrustvenojMreziDataGridViewTextBoxColumn";
            // 
            // CustomersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1064, 681);
            this.Controls.Add(this.PanelCustomersForm);
            this.Name = "CustomersForm";
            this.Text = "CustomersForm";
            this.Load += new System.EventHandler(this.CustomersForm_Load);
            this.PanelCustomersForm.ResumeLayout(false);
            this.PanelCustomersForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kupacBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel PanelCustomersForm;
        private MetroFramework.Controls.MetroGrid metroGrid1;
        private System.Windows.Forms.BindingSource kupacBindingSource;
        private MetroFramework.Controls.MetroButton btnAddNewCustomer;
        private MetroFramework.Controls.MetroTextBox txtSearch;
        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroButton btnAddCustomer;
        private MetroFramework.Controls.MetroComboBox metroComboBox1;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroTextBox txtCustomerPhone;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroTextBox txtCustomerPtt;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroTextBox txtCustomerCity;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroTextBox txtCustomerAddress;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox txtCustomerLastName;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox txtCustomerFirstName;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox txtNickname;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroButton btnCustomerDelete;
        private MetroFramework.Controls.MetroButton btnCustomerEdit;
        private System.Windows.Forms.DataGridViewTextBoxColumn imeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn prezimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn adresaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn gradDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pTTDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn brTelefonaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn drustvenaMrezaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn imeNaDrustvenojMreziDataGridViewTextBoxColumn;
    }
}