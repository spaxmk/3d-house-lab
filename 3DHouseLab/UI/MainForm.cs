﻿using _3DHouseLab.UI;
using _3DHouseLab.UI.Home;
using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _3DHouseLab
{
    public partial class Form3dHouseLab : MetroForm
    {

        private OpenForms opnFrm = new OpenForms();

        public Form3dHouseLab()
        {
            InitializeComponent();
            HomeForm homeForm = new HomeForm();
            opnFrm.OpenChildForm(homeForm, PanelForms);
        }

        
        private void btnCustomer_Click(object sender, EventArgs e)
        {
            CustomersForm childForm = new CustomersForm();
            opnFrm.OpenChildForm(childForm, PanelForms);
        }

        private void btnOrders_Click(object sender, EventArgs e)
        {
            OrdersForm ordersForm = new OrdersForm();
            opnFrm.OpenChildForm(ordersForm, PanelForms);
        }

        private void btnHomeAnalitic_Click(object sender, EventArgs e)
        {
            HomeForm homeForm = new HomeForm();
            opnFrm.OpenChildForm(homeForm, PanelForms);
        }
    }
}
