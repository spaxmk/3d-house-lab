﻿
namespace _3DHouseLab
{
    partial class Form3dHouseLab
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PanelNav = new MetroFramework.Controls.MetroPanel();
            this.btnOrders = new MetroFramework.Controls.MetroButton();
            this.btnCustomer = new MetroFramework.Controls.MetroButton();
            this.PanelForms = new MetroFramework.Controls.MetroPanel();
            this.btnHomeAnalitic = new MetroFramework.Controls.MetroButton();
            this.PanelNav.SuspendLayout();
            this.SuspendLayout();
            // 
            // PanelNav
            // 
            this.PanelNav.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.PanelNav.Controls.Add(this.btnHomeAnalitic);
            this.PanelNav.Controls.Add(this.btnOrders);
            this.PanelNav.Controls.Add(this.btnCustomer);
            this.PanelNav.HorizontalScrollbarBarColor = true;
            this.PanelNav.HorizontalScrollbarHighlightOnWheel = false;
            this.PanelNav.HorizontalScrollbarSize = 10;
            this.PanelNav.Location = new System.Drawing.Point(0, 63);
            this.PanelNav.Name = "PanelNav";
            this.PanelNav.Size = new System.Drawing.Size(200, 727);
            this.PanelNav.TabIndex = 0;
            this.PanelNav.VerticalScrollbarBarColor = true;
            this.PanelNav.VerticalScrollbarHighlightOnWheel = false;
            this.PanelNav.VerticalScrollbarSize = 10;
            // 
            // btnOrders
            // 
            this.btnOrders.BackColor = System.Drawing.Color.White;
            this.btnOrders.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnOrders.Location = new System.Drawing.Point(0, 51);
            this.btnOrders.Name = "btnOrders";
            this.btnOrders.Size = new System.Drawing.Size(200, 51);
            this.btnOrders.TabIndex = 3;
            this.btnOrders.Text = "Porudzbenice";
            this.btnOrders.UseSelectable = true;
            this.btnOrders.Click += new System.EventHandler(this.btnOrders_Click);
            // 
            // btnCustomer
            // 
            this.btnCustomer.BackColor = System.Drawing.Color.White;
            this.btnCustomer.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCustomer.Location = new System.Drawing.Point(0, 0);
            this.btnCustomer.Name = "btnCustomer";
            this.btnCustomer.Size = new System.Drawing.Size(200, 51);
            this.btnCustomer.TabIndex = 2;
            this.btnCustomer.Text = "Kupci";
            this.btnCustomer.UseSelectable = true;
            this.btnCustomer.Click += new System.EventHandler(this.btnCustomer_Click);
            // 
            // PanelForms
            // 
            this.PanelForms.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelForms.HorizontalScrollbarBarColor = true;
            this.PanelForms.HorizontalScrollbarHighlightOnWheel = false;
            this.PanelForms.HorizontalScrollbarSize = 10;
            this.PanelForms.Location = new System.Drawing.Point(206, 63);
            this.PanelForms.Name = "PanelForms";
            this.PanelForms.Size = new System.Drawing.Size(1080, 720);
            this.PanelForms.TabIndex = 1;
            this.PanelForms.VerticalScrollbarBarColor = true;
            this.PanelForms.VerticalScrollbarHighlightOnWheel = false;
            this.PanelForms.VerticalScrollbarSize = 10;
            // 
            // btnHomeAnalitic
            // 
            this.btnHomeAnalitic.BackColor = System.Drawing.Color.White;
            this.btnHomeAnalitic.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnHomeAnalitic.Location = new System.Drawing.Point(0, 102);
            this.btnHomeAnalitic.Name = "btnHomeAnalitic";
            this.btnHomeAnalitic.Size = new System.Drawing.Size(200, 51);
            this.btnHomeAnalitic.TabIndex = 4;
            this.btnHomeAnalitic.Text = "Analitika";
            this.btnHomeAnalitic.UseSelectable = true;
            this.btnHomeAnalitic.Click += new System.EventHandler(this.btnHomeAnalitic_Click);
            // 
            // Form3dHouseLab
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1299, 801);
            this.Controls.Add(this.PanelForms);
            this.Controls.Add(this.PanelNav);
            this.Name = "Form3dHouseLab";
            this.Text = "3D House Lab";
            this.PanelNav.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel PanelNav;
        private MetroFramework.Controls.MetroButton btnCustomer;
        private MetroFramework.Controls.MetroPanel PanelForms;
        private MetroFramework.Controls.MetroButton btnOrders;
        private MetroFramework.Controls.MetroButton btnHomeAnalitic;
    }
}

