﻿using _3DHouseLab.Models.db;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _3DHouseLab.UI
{
    public partial class OrdersForm : Form
    {
        public OrdersForm()
        {
            InitializeComponent();
        }

        private void OrdersForm_Load(object sender, EventArgs e)
        {
            using (ModelsContextDb db = new ModelsContextDb())
            {
                porudzbenicaBindingSource.DataSource = db.ListaPorudzbenica.ToList();
                
            }
        }
    }
}
