﻿
namespace _3DHouseLab.UI
{
    partial class OrdersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.OrdersPanelNav = new MetroFramework.Controls.MetroPanel();
            this.OrdersPanelGrid = new MetroFramework.Controls.MetroPanel();
            this.OrdersGrid = new MetroFramework.Controls.MetroGrid();
            this.btnOrdersDelete = new MetroFramework.Controls.MetroButton();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.porudzbenicaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.porudzbenicaIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KupacId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kupacDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.spisakDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ukupnaCenaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datumPorudzbineDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datumSlanjaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iznosIsplacenDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.iznosPodignutDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.metroPanel1.SuspendLayout();
            this.OrdersPanelNav.SuspendLayout();
            this.OrdersPanelGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OrdersGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.porudzbenicaBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.OrdersPanelGrid);
            this.metroPanel1.Controls.Add(this.OrdersPanelNav);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(0, 0);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(1064, 681);
            this.metroPanel1.TabIndex = 0;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // OrdersPanelNav
            // 
            this.OrdersPanelNav.Controls.Add(this.metroButton1);
            this.OrdersPanelNav.Controls.Add(this.btnOrdersDelete);
            this.OrdersPanelNav.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.OrdersPanelNav.HorizontalScrollbarBarColor = true;
            this.OrdersPanelNav.HorizontalScrollbarHighlightOnWheel = false;
            this.OrdersPanelNav.HorizontalScrollbarSize = 10;
            this.OrdersPanelNav.Location = new System.Drawing.Point(0, 614);
            this.OrdersPanelNav.Name = "OrdersPanelNav";
            this.OrdersPanelNav.Size = new System.Drawing.Size(1064, 67);
            this.OrdersPanelNav.TabIndex = 2;
            this.OrdersPanelNav.VerticalScrollbarBarColor = true;
            this.OrdersPanelNav.VerticalScrollbarHighlightOnWheel = false;
            this.OrdersPanelNav.VerticalScrollbarSize = 10;
            // 
            // OrdersPanelGrid
            // 
            this.OrdersPanelGrid.Controls.Add(this.OrdersGrid);
            this.OrdersPanelGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OrdersPanelGrid.HorizontalScrollbarBarColor = true;
            this.OrdersPanelGrid.HorizontalScrollbarHighlightOnWheel = false;
            this.OrdersPanelGrid.HorizontalScrollbarSize = 10;
            this.OrdersPanelGrid.Location = new System.Drawing.Point(0, 0);
            this.OrdersPanelGrid.Name = "OrdersPanelGrid";
            this.OrdersPanelGrid.Size = new System.Drawing.Size(1064, 614);
            this.OrdersPanelGrid.TabIndex = 3;
            this.OrdersPanelGrid.VerticalScrollbarBarColor = true;
            this.OrdersPanelGrid.VerticalScrollbarHighlightOnWheel = false;
            this.OrdersPanelGrid.VerticalScrollbarSize = 10;
            // 
            // OrdersGrid
            // 
            this.OrdersGrid.AllowUserToResizeRows = false;
            this.OrdersGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.OrdersGrid.AutoGenerateColumns = false;
            this.OrdersGrid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.OrdersGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.OrdersGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.OrdersGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.OrdersGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.OrdersGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.OrdersGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.porudzbenicaIdDataGridViewTextBoxColumn,
            this.KupacId,
            this.kupacDataGridViewTextBoxColumn,
            this.spisakDataGridViewTextBoxColumn,
            this.ukupnaCenaDataGridViewTextBoxColumn,
            this.datumPorudzbineDataGridViewTextBoxColumn,
            this.datumSlanjaDataGridViewTextBoxColumn,
            this.iznosIsplacenDataGridViewCheckBoxColumn,
            this.iznosPodignutDataGridViewCheckBoxColumn});
            this.OrdersGrid.DataSource = this.porudzbenicaBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.OrdersGrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.OrdersGrid.EnableHeadersVisualStyles = false;
            this.OrdersGrid.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.OrdersGrid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.OrdersGrid.Location = new System.Drawing.Point(12, 12);
            this.OrdersGrid.Name = "OrdersGrid";
            this.OrdersGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.OrdersGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.OrdersGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.OrdersGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.OrdersGrid.Size = new System.Drawing.Size(1040, 596);
            this.OrdersGrid.TabIndex = 2;
            // 
            // btnOrdersDelete
            // 
            this.btnOrdersDelete.Location = new System.Drawing.Point(938, 11);
            this.btnOrdersDelete.Name = "btnOrdersDelete";
            this.btnOrdersDelete.Size = new System.Drawing.Size(114, 44);
            this.btnOrdersDelete.TabIndex = 2;
            this.btnOrdersDelete.Text = "Obrisi";
            this.btnOrdersDelete.UseSelectable = true;
            // 
            // metroButton1
            // 
            this.metroButton1.Location = new System.Drawing.Point(809, 11);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(114, 44);
            this.metroButton1.TabIndex = 3;
            this.metroButton1.Text = "Izmeni";
            this.metroButton1.UseSelectable = true;
            // 
            // porudzbenicaBindingSource
            // 
            this.porudzbenicaBindingSource.DataSource = typeof(_3DHouseLab.Models.Porudzbenica);
            // 
            // porudzbenicaIdDataGridViewTextBoxColumn
            // 
            this.porudzbenicaIdDataGridViewTextBoxColumn.DataPropertyName = "PorudzbenicaId";
            this.porudzbenicaIdDataGridViewTextBoxColumn.HeaderText = "PorudzbenicaId";
            this.porudzbenicaIdDataGridViewTextBoxColumn.Name = "porudzbenicaIdDataGridViewTextBoxColumn";
            // 
            // KupacId
            // 
            this.KupacId.DataPropertyName = "KupacId";
            this.KupacId.HeaderText = "KupacId";
            this.KupacId.Name = "KupacId";
            // 
            // kupacDataGridViewTextBoxColumn
            // 
            this.kupacDataGridViewTextBoxColumn.DataPropertyName = "Kupac";
            this.kupacDataGridViewTextBoxColumn.HeaderText = "Kupac";
            this.kupacDataGridViewTextBoxColumn.Name = "kupacDataGridViewTextBoxColumn";
            // 
            // spisakDataGridViewTextBoxColumn
            // 
            this.spisakDataGridViewTextBoxColumn.DataPropertyName = "Spisak";
            this.spisakDataGridViewTextBoxColumn.HeaderText = "Spisak";
            this.spisakDataGridViewTextBoxColumn.Name = "spisakDataGridViewTextBoxColumn";
            // 
            // ukupnaCenaDataGridViewTextBoxColumn
            // 
            this.ukupnaCenaDataGridViewTextBoxColumn.DataPropertyName = "UkupnaCena";
            this.ukupnaCenaDataGridViewTextBoxColumn.HeaderText = "UkupnaCena";
            this.ukupnaCenaDataGridViewTextBoxColumn.Name = "ukupnaCenaDataGridViewTextBoxColumn";
            // 
            // datumPorudzbineDataGridViewTextBoxColumn
            // 
            this.datumPorudzbineDataGridViewTextBoxColumn.DataPropertyName = "DatumPorudzbine";
            this.datumPorudzbineDataGridViewTextBoxColumn.HeaderText = "DatumPorudzbine";
            this.datumPorudzbineDataGridViewTextBoxColumn.Name = "datumPorudzbineDataGridViewTextBoxColumn";
            // 
            // datumSlanjaDataGridViewTextBoxColumn
            // 
            this.datumSlanjaDataGridViewTextBoxColumn.DataPropertyName = "DatumSlanja";
            this.datumSlanjaDataGridViewTextBoxColumn.HeaderText = "DatumSlanja";
            this.datumSlanjaDataGridViewTextBoxColumn.Name = "datumSlanjaDataGridViewTextBoxColumn";
            // 
            // iznosIsplacenDataGridViewCheckBoxColumn
            // 
            this.iznosIsplacenDataGridViewCheckBoxColumn.DataPropertyName = "IznosIsplacen";
            this.iznosIsplacenDataGridViewCheckBoxColumn.HeaderText = "IznosIsplacen";
            this.iznosIsplacenDataGridViewCheckBoxColumn.Name = "iznosIsplacenDataGridViewCheckBoxColumn";
            // 
            // iznosPodignutDataGridViewCheckBoxColumn
            // 
            this.iznosPodignutDataGridViewCheckBoxColumn.DataPropertyName = "IznosPodignut";
            this.iznosPodignutDataGridViewCheckBoxColumn.HeaderText = "IznosPodignut";
            this.iznosPodignutDataGridViewCheckBoxColumn.Name = "iznosPodignutDataGridViewCheckBoxColumn";
            // 
            // OrdersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1064, 681);
            this.Controls.Add(this.metroPanel1);
            this.Name = "OrdersForm";
            this.Text = "OrdersForm";
            this.Load += new System.EventHandler(this.OrdersForm_Load);
            this.metroPanel1.ResumeLayout(false);
            this.OrdersPanelNav.ResumeLayout(false);
            this.OrdersPanelGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.OrdersGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.porudzbenicaBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroPanel OrdersPanelGrid;
        private MetroFramework.Controls.MetroGrid OrdersGrid;
        private System.Windows.Forms.BindingSource porudzbenicaBindingSource;
        private MetroFramework.Controls.MetroPanel OrdersPanelNav;
        private MetroFramework.Controls.MetroButton btnOrdersDelete;
        private MetroFramework.Controls.MetroButton metroButton1;
        private System.Windows.Forms.DataGridViewTextBoxColumn porudzbenicaIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn KupacId;
        private System.Windows.Forms.DataGridViewTextBoxColumn kupacDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn spisakDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ukupnaCenaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn datumPorudzbineDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn datumSlanjaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn iznosIsplacenDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn iznosPodignutDataGridViewCheckBoxColumn;
    }
}