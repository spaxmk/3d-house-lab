﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace _3DHouseLab.UI
{
    public class OpenForms
    {
        public Form currentChildForm;

        public void OpenChildForm(Form childForm, Panel panels)
        {
            if (currentChildForm != null)
            {
                currentChildForm.Close();
            }
            currentChildForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            panels.Controls.Add(childForm);
            panels.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
        }
    }
}
