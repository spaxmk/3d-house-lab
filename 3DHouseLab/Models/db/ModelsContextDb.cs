﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;

namespace _3DHouseLab.Models.db
{
    public class ModelsContextDb : DbContext
    {
        public ModelsContextDb() : base("name=Db3dHouseLab")
        {

        }

        public DbSet<Kupac> ListaKupaca { get; set; }
        public DbSet<Porudzbenica> ListaPorudzbenica { get; set; }
    }
}
