﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3DHouseLab.Models
{
    public class Porudzbenica
    {
        [Key]
        public int PorudzbenicaId { get; set; }
        public string Spisak { get; set; }
        public int UkupnaCena { get; set; }
        public string DatumPorudzbine { get; set; }
        public string DatumSlanja { get; set; }
        public bool IznosIsplacen { get; set; }
        public bool IznosPodignut { get; set; }

        [ForeignKey("Kupac")]
        public int KupacId { get; set; }
        public Kupac Kupac { get; set; }
    }
}
