﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3DHouseLab.Models
{
    public class Kupac
    {
        [Key]
        public int KupacId { get; set; }
        [StringLength(255)]
        public string Ime { get; set; }
        [StringLength(255)]
        public string Prezime { get; set; }
        [StringLength(255)]
        public string Adresa { get; set; }
        [StringLength(255)]
        public string Grad { get; set; }
        public int PTT { get; set; }
        [StringLength(11)]
        public string BrTelefona { get; set; }
        [StringLength(255)]
        public string DrustvenaMreza { get; set; }
        [StringLength(255)]
        public string ImeNaDrustvenojMrezi { get; set; }

        //[NotMapped]
        //public int ObjectState { get; set; }
    }
}
